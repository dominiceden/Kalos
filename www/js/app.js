// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ui.bootstrap'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) { // set routes
  $stateProvider
    .state('tabs', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })
    .state ('tabs.home', { // home is a subtemplate of tabs. It's a subtemplate because tabs controls which view is loaded and there are many views within one tab view.
      url: '/home', // because this is a subview of tabs, the URL is #/tab/home
      views: {
        'home-tab': { // Note that the key of this object is referenced in the HTML as <ion-nav-view name="home-tab">
          templateUrl: 'templates/home.html', // load the home.html template into the view
          controller: 'HomeController'
        }
      }
    })
    .state ('tabs.workout', { // home is a subtemplate of tabs. It's a subtemplate because tabs controls which view is loaded and there are many views within one tab view.
      url: '/home/:aId', // because this is a subview of tabs, the URL is #/tabs/home/:aId
      views: {
        'home-tab': {
          templateUrl: 'templates/workout.html',
          controller: 'HomeController'
        }
      }
    })

    $urlRouterProvider.otherwise('tab/home'); // default URL
})

// Define the controller. This holds all the JS that the view needs to work as intended.

.controller('HomeController', ['$scope', '$http', '$state', function($scope, $http, $state) {
  $http.get('js/data.json').success(function(data) {
    $scope.workouts = data.workouts; // create a variable called workouts in the current scope.
    $scope.whichworkout = $state.params.aId;
  });
}])

.controller('WorkoutController', ['$scope', '$http', '$state', function($scope, $http, $state) {
  $http.get('js/data.json').success(function(data) {

  // find the current workout in the object
  for (i = 0; i < data.workouts.length; i++) {
    if (data.workouts[i].url == $state.params.aId) { // If the url of the workout is matched by the current URL of the workout
      $scope.steps = data.workouts[i].steps; // Set the scope variable steps to the steps for this workout. We can then iterate over this in the view using ng-repeat.
    };
  }
  $scope.isCollapsed = true;

  });
}])
